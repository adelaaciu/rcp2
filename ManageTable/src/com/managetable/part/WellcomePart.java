package com.managetable.part;

import javax.annotation.PostConstruct;

import org.eclipse.e4.ui.internal.workbench.PartServiceImpl;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class WellcomePart {

	@PostConstruct
	public void createPart(Composite parent, PartServiceImpl partService) {

		parent.setLayout(new GridLayout(1, false));

		Button displayTableBtn = new Button(parent, SWT.NONE);
		displayTableBtn.setText("Persons");

		displayTableBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				MPart part = partService.findPart("TablePart");
				part.setVisible(true);
				partService.getActivePart().setVisible(false);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
	}

}
