package com.managetable.part;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.managetable.handlers.AddHandler;
import com.managetable.handlers.DeleteHandler;
import com.managetable.model.Person;

public class TablePart {

	@Inject
	private Shell shell;

	@Inject
	private Composite parent;
	
	private TableViewer viewer;
	private List<Person> personLists = new ArrayList<>();
	private CellEditor editor;

	@PostConstruct
	public void CreateView() {

		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);

		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 4;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

		createColumns();
		addContent();

		Button addBtn = new Button(parent, SWT.NONE);
		Button deleteBtn = new Button(parent, SWT.NONE);

		addBtn.setText("Add");
		deleteBtn.setText("Delete");

		// add Listener to buttons
		addBtn.addSelectionListener(new AddHandler(viewer, personLists));
		deleteBtn.addSelectionListener(new DeleteHandler(viewer, personLists));

		addBtn.setToolTipText("Fill in the fields on the last row with valid value");
		deleteBtn.setToolTipText("Can't delete last field");
		
		this.editor = new TextCellEditor(viewer.getTable());

	}

	private void createColumns() {

		TableViewerColumn firstName = new TableViewerColumn(viewer, SWT.None);
		firstName.getColumn().setText("First Name");
		firstName.getColumn().setWidth(100);

		firstName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;

				return p.getFirstName();
			}
		});

		firstName.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				String firstName = String.valueOf(value);

				((Person) element).setFirstName(firstName);
				viewer.refresh();
			}

			@Override
			protected Object getValue(Object element) {
				return ((Person) element).getFirstName();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return editor;
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

		TableViewerColumn lastName = new TableViewerColumn(viewer, SWT.None);

		lastName.getColumn().setText("Last Name");
		lastName.getColumn().setWidth(100);
		lastName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;

				return p.getLastName();
			}
		});

		lastName.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				String lastName = String.valueOf(value);

				((Person) element).setLastName(lastName);
				viewer.refresh();
			}

			@Override
			protected Object getValue(Object element) {
				return ((Person) element).getLastName();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return editor;
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

		TableViewerColumn age = new TableViewerColumn(viewer, SWT.None);

		age.getColumn().setText("Age");
		age.getColumn().setWidth(100);
		age.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;

				return String.valueOf(p.getAge());
			}
		});

		age.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object age) {
				try {
					((Person) element).setAge(Integer.parseInt(String.valueOf(age)));
				} catch (Exception e) {
					MessageDialog.openWarning(shell, "Error", "Please insert a number");
				}
				viewer.refresh();
			}

			@Override
			protected Object getValue(Object element) {
				return (Object) ((Person) element).getAge().toString();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return editor;
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});

		TableViewerColumn cnp = new TableViewerColumn(viewer, SWT.None);

		cnp.getColumn().setText("CNP");
		cnp.getColumn().setWidth(100);
		cnp.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;

				return String.valueOf(p.getCnp());
			}
		});

		cnp.setEditingSupport(new EditingSupport(viewer) {

			String cnp;

			@Override
			protected void setValue(Object element, Object value) {
				try {
					cnp = String.valueOf(value);
					if (cnp.length() != 13) {
						MessageDialog.openWarning(shell, "Warning", "Please insert a 13 digit number");
					} else
						((Person) element).setCnp(Long.parseLong(cnp));

				} catch (Exception e) {
					MessageDialog.openWarning(shell, "Error", "Please insert a 13 digit number");
				}
				viewer.refresh();
			}

			@Override
			protected Object getValue(Object element) {
				return (Object) ((Person) element).getCnp().toString();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				return editor;
			}

			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
	}

	private void addContent() {

		personLists.add(new Person("Adela", "Aciu", 5, 2950506355966L));
		personLists.add(new Person("Amalia", "Aciu", 10, 4478963210122L));
		personLists.add(Person.prototype());
		viewer.setInput(personLists);
	}

}
