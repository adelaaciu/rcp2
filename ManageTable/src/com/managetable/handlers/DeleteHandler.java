package com.managetable.handlers;

import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.managetable.model.Person;

public class DeleteHandler implements SelectionListener {

	private TableViewer viewer;
	private List<Person> personLists;

	public DeleteHandler(TableViewer viewer, List<Person> personLists) {
		this.viewer = viewer;
		this.personLists = personLists;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		int index = viewer.getTable().getSelectionIndex();
		if (index != personLists.size() - 1) {
			personLists.remove(index);
			viewer.refresh();
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

}
