package com.example.swt.widgets.dragdrop;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class PhotoShuffler {

	public static void main(String[] args) {

		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setSize(520, 200);
		shell.setLayout(new RowLayout());
		shell.setText("Photo Shuffler");

		Composite parent = new Composite(shell, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		parent.setLayout(gridLayout);

		 String path = System.getProperty("user.dir") + "/images/";
		 
		   String[] imgNames = new String[] { "1.png", "2.png",
	                "3.png", "4.png" };
		 
		 
        for (int i = 0; i < imgNames.length; i++) {

        	Label label = new Label(parent, SWT.NONE);
            Image img = new Image(display, path + imgNames[i]);
            label.setImage(img);

            DragSource source = new DragSource(label, DND.DROP_NONE);
            source.setTransfer(new Transfer[] { TextTransfer.getInstance() });
            source.addDragListener(new MyDragSourceListener(parent, source));

            DropTarget target = new DropTarget(label, DND.DROP_NONE);
            target.setTransfer(new Transfer[] { TextTransfer.getInstance() });
            target.addDropListener(new MyDropTargetListener(parent, target));
                       
        }
		
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

}
