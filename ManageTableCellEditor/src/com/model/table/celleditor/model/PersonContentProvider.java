package com.model.table.celleditor.model;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;

public class PersonContentProvider implements IStructuredContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List) {
			List<Person> list = (List<Person>) inputElement;
			return list.toArray();
		}

		return new Object[0];

	}
}
