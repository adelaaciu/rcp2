package com.model.table.celleditor.model;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Item;

import com.model.table.celleditor.part.TablePart;

/**
 * Modificari aduse campurilor
 *
 */
public class PersonCellModifier implements ICellModifier {

	private Viewer viewer;

	public PersonCellModifier(Viewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public boolean canModify(Object element, String property) {
		return true;
	}

	@Override
	public Object getValue(Object element, String property) {

		Person person = (Person) element;
		if (TablePart.FIRST_NAME.equals(property)) {
			return person.getFirstName();
		} else if (TablePart.LAST_NAME.equals(property)) {
			return person.getLastName();
		} else if (TablePart.AGE.equals(property)) {
			return person.getAge().toString();
		} else if (TablePart.CNP.equals(property)) {
			return person.getCnp().toString();
		}

		return null;
	}

	@Override
	public void modify(Object element, String property, Object value) {

		if (element instanceof Item)
			element = ((Item) element).getData();

		Person person = (Person) element;
		if (TablePart.FIRST_NAME.equals(property)) {
			person.setFirstName(String.valueOf(value));
		} else if (TablePart.LAST_NAME.equals(property)) {
			person.setLastName(String.valueOf(value));
		} else if (TablePart.AGE.equals(property)) {
			try {
				Integer age = Integer.valueOf(String.valueOf(value));
				person.setAge(age);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
			}
		} else if (TablePart.CNP.equals(property)) {

			try {
				person.setCnp(Long.valueOf(String.valueOf(value)));
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		viewer.refresh();

	}

}
