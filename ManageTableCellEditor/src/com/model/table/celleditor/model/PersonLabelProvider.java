package com.model.table.celleditor.model;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class PersonLabelProvider extends LabelProvider implements ITableLabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * In functie de coloana sunt preluate campurile corespunzatoare
	 */
	@Override
	public String getColumnText(Object element, int columnIndex) {

		if (!(element instanceof Person)) {
			return "";
		}

		Person person = (Person) element;

		switch (columnIndex) {
		case 0:
			return person.getFirstName();
		case 1:
			return person.getLastName();

		case 2:
			return person.getAge().toString();

		case 3:
			return person.getCnp().toString();
		}

		return "";
	}

}
