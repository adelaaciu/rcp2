package com.model.table.celleditor.part;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.model.table.celleditor.handlers.AddHandler;
import com.model.table.celleditor.handlers.DeleteHandler;
import com.model.table.celleditor.model.Person;
import com.model.table.celleditor.model.PersonCellModifier;
import com.model.table.celleditor.model.PersonContentProvider;
import com.model.table.celleditor.model.PersonLabelProvider;

public class TablePart {

	@Inject
	private Composite parent;

	private TableViewer viewer;
	private Table table;
	private List<Person> personLists;

	public static final String FIRST_NAME = "First Name";
	public static final String LAST_NAME = "Last Name";
	public static final String AGE = "Age";
	public static final String CNP = "CNP";
	public static final String[] PROPS = { FIRST_NAME, LAST_NAME, AGE, CNP };

	@PostConstruct
	public void CreateView() {
		personLists = new ArrayList<>();

		Composite composite = new Composite(parent, SWT.NONE);

		GridLayout layout = new GridLayout(2, false);
		composite.setLayout(layout);

		viewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;

		viewer.getTable().setLayoutData(gridData);
		viewer.setContentProvider(new PersonContentProvider());
		viewer.setLabelProvider(new PersonLabelProvider());

		addContent();
		createColumns();

		Button addBtn = new Button(composite, SWT.CENTER);
		Button deleteBtn = new Button(composite, SWT.CENTER);

		addBtn.setText("Add");
		deleteBtn.setText("Delete");

		// add Listener to buttons
		addBtn.addSelectionListener(new AddHandler(viewer, personLists));
		deleteBtn.addSelectionListener(new DeleteHandler(viewer, personLists));

		addBtn.setToolTipText("Fill in the fields on the last row with valid value");
		deleteBtn.setToolTipText("Can't delete last field");

	}

	private void createColumns() {
		table = viewer.getTable();

		TableColumn firstName = new TableColumn(viewer.getTable(), SWT.CENTER);
		firstName.setText(FIRST_NAME);
		firstName.setWidth(100);

		TableColumn lastName = new TableColumn(viewer.getTable(), SWT.CENTER);
		lastName.setText(LAST_NAME);
		lastName.setWidth(100);

		TableColumn age = new TableColumn(viewer.getTable(), SWT.CENTER);
		age.setText(AGE);
		age.setWidth(100);

		TableColumn cnp = new TableColumn(viewer.getTable(), SWT.CENTER);
		cnp.setText(CNP);
		cnp.setWidth(100);

		viewer.setColumnProperties(PROPS);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		CellEditor[] editors = new CellEditor[4];
		editors[0] = new TextCellEditor(table);
		editors[1] = new TextCellEditor(table);
		editors[2] = new TextCellEditor(table);
		editors[3] = new TextCellEditor(table);

		viewer.setCellEditors(editors);
		viewer.setCellModifier(new PersonCellModifier(viewer));

		viewer.refresh();
	}

	private void addContent() {

		viewer.setInput(personLists);

		personLists.add(new Person("Adela", "Aciu", 5, 2950506355966L));
		personLists.add(new Person("Amalia", "Aciu", 10, 4478963210122L));
		personLists.add(Person.prototype());

		viewer.refresh();
	}

	@Focus
	public void setFocus() {
		viewer.getTable().setFocus();
	}
}
