package com.model.table.celleditor.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.internal.workbench.PartServiceImpl;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class WellcomePart {

	@Inject
	private Composite parent;

	@PostConstruct
	public void createPart(PartServiceImpl partService) {

		parent.setLayout(new GridLayout(1, false));

		Button displayTableBtn = new Button(parent, SWT.NONE);
		displayTableBtn.setText("Persons");

		displayTableBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				MPart part = partService.findPart("ManagePersonPart");
				part.setVisible(true);
				partService.getActivePart().setVisible(false);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
	}

	@Focus
	public void setFocus() {
		parent.setFocus();
	}
}
