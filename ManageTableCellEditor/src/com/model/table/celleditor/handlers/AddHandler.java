package com.model.table.celleditor.handlers;

import java.util.List;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.model.table.celleditor.model.Person;

public class AddHandler implements SelectionListener {
	private TableViewer viewer;
	private List<Person> personLists;

	public AddHandler(TableViewer viewer, List<Person> personLists) {
		this.viewer = viewer;
		this.personLists = personLists;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {

		if (!viewer.getSelection().isEmpty()) {
			Person selected = (Person) ((StructuredSelection) viewer.getSelection()).iterator().next();

			if (selected.getCnp() != 0 && !selected.getFirstName().equals(Person.prototype().getFirstName())
					&& !selected.getLastName().equals(Person.prototype().getLastName())) {
				personLists.remove(personLists.size() - 1);
				personLists.add(selected);
				personLists.add(Person.prototype());
				viewer.refresh();
			}
		}

	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {

	}
}
