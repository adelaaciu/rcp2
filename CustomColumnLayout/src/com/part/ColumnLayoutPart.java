package com.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

import com.layout.ColumnLayout;

public class ColumnLayoutPart {

	private Button button3;

	@Inject
	private Shell shell;

	@PostConstruct
	private void addComponents() {
		shell.setLayout(new ColumnLayout());

		new Button(shell, SWT.PUSH).setText("B1");
		new Button(shell, SWT.PUSH).setText("Very Wide Button 2");

		(button3 = new Button(shell, SWT.PUSH)).setText("Button 3");

		Button grow = new Button(shell, SWT.PUSH);
		grow.setText("Grow Button 3");
		grow.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				button3.setText("Extreemely Wide Button 3");
				shell.layout();
				shell.pack();
			}
		});

		Button shrink = new Button(shell, SWT.PUSH);
		shrink.setText("Shrink Button 3");
		shrink.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				button3.setText("Button 3");
				shell.layout();
				shell.pack();
			}
		});
	}
}
