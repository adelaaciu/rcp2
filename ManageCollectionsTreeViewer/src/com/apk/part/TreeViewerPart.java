package com.apk.part;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.apk.handler.AddHandler;
import com.apk.handler.DeleteHandler;
import com.apk.model.ChangeNameLater;
import com.apk.model.Company;
import com.apk.model.Person;
import com.apk.provider.CompanyOwnerLabelProvider;
import com.apk.provider.PersonAgeLabelProvider;
import com.apk.provider.PersonCnpLabelProvider;
import com.apk.provider.NameLabelProvider;
import com.apk.provider.ViewContentProvider;

public class TreeViewerPart {

	@Inject
	private Composite parent;

	private Text personNameText;
	private Text ageText;
	private Text cnpText;
	private Text ownerText;
	private TreeViewer viewer;
	private List<ChangeNameLater> changeNameList = new ArrayList<>();

	private static final String NAME = "Name";
	private static final String AGE = "Age";
	private static final String CNP = "CNP";
	private static final String OWNER = "Owner";
	private static final String[] PROPERTIES = { NAME, AGE, CNP, OWNER };

	@PostConstruct
	public void create() {
		parent.setLayout(new GridLayout(4, false));

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FILL);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 4;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;

		Tree tree = viewer.getTree();
		tree.setLayoutData(gridData);
		viewer.setContentProvider(new ViewContentProvider());

		tree.setHeaderVisible(true);

		addComponentsToTreeViewer(viewer);
		addContent();
		// viewer.setInput(changeNameList);
		addCellModifier(tree);

		Button addBtn = new Button(parent, SWT.CENTER);
		addBtn.setText("Add");
		addBtn.addSelectionListener(
				new AddHandler(viewer, personNameText, ageText, cnpText, ownerText, changeNameList));

		/**
		 * try2
		 */
		// addBtn.addSelectionListener(new AddHandler(viewer,
		// personNameText.getText(), ageText.getText(),
		// cnpText.getText(), ownerText.getText(), changeNameList));

		Button deleteBtn = new Button(parent, SWT.CENTER);
		deleteBtn.setText("Delete");
		deleteBtn.addSelectionListener(new DeleteHandler(viewer, changeNameList));

		// when double click the editing happen

		TreeViewerEditor.create(viewer, new ColumnViewerEditorActivationStrategy(viewer) {
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.MOUSE_DOUBLE_CLICK_SELECTION;
			}
		}, 0);

	}

	private void addCellModifier(Tree tree) {

		viewer.setColumnProperties(PROPERTIES);
		viewer.setInput(changeNameList);

		CellEditor[] editors = new CellEditor[5];

		editors[0] = new TextCellEditor(tree);
		editors[1] = new TextCellEditor(tree);
		editors[2] = new TextCellEditor(tree);
		editors[3] = new TextCellEditor(tree);
		editors[4] = new TextCellEditor(tree);

		viewer.setCellEditors(editors);

		viewer.setCellModifier(new ICellModifier() {

			@Override
			public void modify(Object element, String property, Object value) {
				Person p = null;
				Company c = null;
				TreeItem treeItem = (TreeItem) element;
				if (treeItem.getData() instanceof Person)
					p = (Person) treeItem.getData();
				else
					c = (Company) treeItem.getData();
				switch (property) {
				case NAME:
					if (p != null)
						p.setName(value.toString());
					else
						c.setName(value.toString());
					break;

				case AGE:
					try {
						p.setAge(Integer.parseInt(value.toString()));
					} catch (Exception e) {
						// TODO: handle exception
					}
					break;
				case CNP:
					try {
						if (value.toString().length() == 13)
							p.setCnp(Long.parseLong(value.toString()));
					} catch (Exception e) {
						// TODO: handle exception
					}

					break;

				case OWNER:

					if (c != null)
						c.setOwner(value.toString());
					break;
				}

				if (p != null)
					viewer.update(p, PROPERTIES);
				else
					viewer.update(c, PROPERTIES);

			}

			@Override
			public Object getValue(Object element, String property) {

				Person p = null;
				Company c = null;

				if (element instanceof Person) {
					p = (Person) element;
				} else {
					c = (Company) element;
				}

				switch (property) {
				case NAME:
					if (p != null)
						return p.getName();
					else
						return c.getName();
				case AGE:
					try {
						return p.getAge().toString();
					} catch (Exception e) {
						return " ";
					}
				case CNP:
					try {
						return p.getCnp().toString();
					} catch (Exception e) {
						return " ";
					}

				case OWNER:

					if (c != null)
						return c.getOwner();

				}

				return " ";
			}

			@Override
			public boolean canModify(Object element, String property) {
				if (element instanceof Person) {

					switch (property) {
					case NAME:
						return true;
					case AGE:
						return true;
					case CNP:
						return true;
					default:
						return false;
					}
				} else
					switch (property) {
					case NAME:
						return true;
					case OWNER:
						return true;
					default:
						return false;
					}

			}
		});
	}

	private void addComponentsToTreeViewer(TreeViewer viewer) {
		TreeViewerColumn mainColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		mainColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new NameLabelProvider()));
		mainColumn.getColumn().setText(NAME);
		mainColumn.getColumn().setWidth(150);

		TreeViewerColumn ageColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		ageColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new PersonAgeLabelProvider()));
		ageColumn.getColumn().setText(AGE);
		ageColumn.getColumn().setWidth(70);

		TreeViewerColumn cnpColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		cnpColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new PersonCnpLabelProvider()));
		cnpColumn.getColumn().setText(CNP);
		cnpColumn.getColumn().setWidth(150);

		TreeViewerColumn ownerColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		ownerColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new CompanyOwnerLabelProvider()));
		ownerColumn.getColumn().setText(OWNER);
		ownerColumn.getColumn().setWidth(100);

		new Label(parent, SWT.CENTER).setText("Name");
		personNameText = new Text(parent, SWT.CENTER);
		personNameText.setTextLimit(50);

		new Label(parent, SWT.CENTER).setText("Age");
		ageText = new Text(parent, SWT.CENTER);
		ageText.setTextLimit(3);

		new Label(parent, SWT.CENTER).setText("CNP");
		cnpText = new Text(parent, SWT.CENTER);
		cnpText.setTextLimit(13);

		new Label(parent, SWT.CENTER).setText("Owner");
		ownerText = new Text(parent, SWT.CENTER);

		/**
		 * Foreach column
		 */
		// setEditingSuport(viewer, mainColumn, ageColumn, cnpColumn,
		// ownerColumn);
	}

	private void setEditingSuport(TreeViewer viewer, TreeViewerColumn mainColumn, TreeViewerColumn ageColumn,
			TreeViewerColumn cnpColumn, TreeViewerColumn ownerColumn) {
		mainColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {

				if (element instanceof Person) {

					Person p = (Person) element;
					p.setName(String.valueOf(value));
				} else {
					Company company = (Company) element;
					company.setName(String.valueOf(value));
				}

				getViewer().update(element, null);

			}

			@Override
			protected Object getValue(Object element) {
				if (element instanceof Person) {
					return ((Person) element).getName();
				} else {
					Company company = (Company) element;

					return company.getName();
				}
			}

			@Override
			protected CellEditor getCellEditor(Object element) { //
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) { // TODO
				return true;
			}
		});

		ageColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				if (element instanceof Person) {
					Person p = (Person) element;
					p.setAge(Integer.parseInt(String.valueOf(value)));
					getViewer().update(element, null);
				}

			}

			@Override
			protected Object getValue(Object element) {
				if (element instanceof Person) {
					return ((Person) element).getAge().toString();
				} else
					return " ";
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) {
				if (element instanceof Person)
					return true;
				else
					return false;

			}
		});

		cnpColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				if (element instanceof Person) {
					Person p = (Person) element;
					p.setCnp(Long.parseLong(String.valueOf(value)));
					getViewer().update(element, null);
				}
			}

			@Override
			protected Object getValue(Object element) {
				if (element instanceof Person) {
					return ((Person) element).getCnp().toString();
				} else
					return " ";
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) {
				if (element instanceof Person)
					return true;
				else
					return false;
			}
		});

		ownerColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				if (element instanceof Company) {
					Company company = (Company) element;
					company.setOwner(String.valueOf(value));
					getViewer().update(element, null);
				}
			}

			@Override
			protected Object getValue(Object element) {
				if (element instanceof Company) {
					Company company = (Company) element;

					return company.getOwner();
				}

				return " ";
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) {

				if (element instanceof Company)
					return true;
				else
					return false;
			}
		});

	}

	private void addContent() {
		Person p = new Person("Adela Aciu", 21, 2950425056066L);

		Person p1 = new Person("Amalia Aciu", 21, 2950101076066L);
		Person p2 = new Person("Alex Aciu", 23, 1950101076066L);
		Person p3 = new Person("Marian Onac", 22, 1951006076066L);
		Person p5 = new Person("George Onac", 22, 1951006076066L);

		List<Person> friends = new ArrayList<>();
		friends.add(p1);
		friends.add(p2);
		friends.add(p3);

		p.setPersonFriends(friends);

		p1.setParent(p);
		p2.setParent(p);
		p3.setParent(p);

		friends = new ArrayList<>();
		friends.add(p5);
		p3.setPersonFriends(friends);
		p5.setParent(p3);

		Company c = new Company("Company", "Company");

		changeNameList.add(c);
		changeNameList.add(p);
	}

}
