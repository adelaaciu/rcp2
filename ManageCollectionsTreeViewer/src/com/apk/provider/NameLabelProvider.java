package com.apk.provider;

import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;

import com.apk.model.Company;
import com.apk.model.Person;

public class NameLabelProvider extends LabelProvider implements IStyledLabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
	}

	@Override
	public StyledString getStyledText(Object element) {
		String name;

		if (element instanceof Person) {
			name = ((Person) element).getName();
			return new StyledString(name);
		} else {
			name = ((Company) element).getName();
			return new StyledString(name);
		}
	}

	@Override
	public Image getImage(Object element) {
		return null;
	}

}
