package com.apk.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;

import com.apk.model.Company;

public class CompanyOwnerLabelProvider extends LabelProvider implements IStyledLabelProvider {

	@Override
	public StyledString getStyledText(Object element) {

		if (element instanceof Company) {

			Company company = (Company) element;
			return new StyledString(company.getOwner());
		} else
			return new StyledString(" ");
	}
}