package com.apk.provider;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.apk.model.ChangeNameLater;
import com.apk.model.Company;
import com.apk.model.Person;

public class ViewContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		List<Person> list = (List<Person>) inputElement;
		return list.toArray();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		ChangeNameLater parent;

		if (parentElement instanceof Person) {
			Person person = (Person) parentElement;
			return person.getPersonFriends().toArray();
		} else {
			Company company = (Company) parentElement;
			return company.getCompanies().toArray();
		}
	}

	@Override
	public Object getParent(Object element) {

		if (element instanceof Person) {
			Person person = (Person) element;
			return person.getParent();
		} else {
			Company company = (Company) element;
			return company.getParent();
		}

	}

	@Override
	public boolean hasChildren(Object element) {

		if (element instanceof Person) {
			Person person = (Person) element;
			return !person.getPersonFriends().isEmpty();
		} else {
			Company company = (Company) element;
			return !company.getCompanies().isEmpty();
		}

	}
}
