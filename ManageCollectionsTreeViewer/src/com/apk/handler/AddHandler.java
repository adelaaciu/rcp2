package com.apk.handler;

import java.util.List;

import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Text;

import com.apk.model.ChangeNameLater;
import com.apk.model.Company;
import com.apk.model.Person;

public class AddHandler implements SelectionListener {

	private Person person;
	private Company company;
	private TreeViewer viewer;

	private Text name;
	private Text age;
	private Text cnp;
	private Text owner;

	private List<ChangeNameLater> list;

	public AddHandler(TreeViewer viewer, Text name, Text age, Text cnp, Text owner, List<ChangeNameLater> list) {
		this.viewer = viewer;
		this.name = name;
		this.age = age;
		this.cnp = cnp;
		this.list = list;
		this.owner = owner;
	}

	private void create() {
		if (!name.getText().isEmpty()) {
			if (cnp.getText().length() == 13 && !age.getText().isEmpty()) {
				try {
					person = new Person(name.getText().toString(), Integer.parseInt(String.valueOf(age.getText())),
							Long.parseLong(String.valueOf(cnp.getText())));
				} catch (Exception e) {
					person = null;
				}
			} else {

				person = null;

				if (!owner.getText().isEmpty()) {
					company = new Company(name.getText(), owner.getText());
				} else
					company = null;
			}

		}
	}

	@Override
	public void widgetSelected(SelectionEvent e) {

		create();
		if (person != null || company != null)
			if (list.isEmpty()) {
				addContent();

			} else {

				TreeSelection selection = (TreeSelection) viewer.getSelection();

				if (selection.isEmpty())
					addContent();
				else {
					if (selection.iterator().next() instanceof Person && person != null) {

						Person p = (Person) selection.iterator().next();
						person.setParent(p);
						p.getPersonFriends().add(person);
					} else if (selection.iterator().next() instanceof Company && company != null) {

						Company c = (Company) selection.iterator().next();
						company.setParent(c);
						c.getCompanies().add(company);

					}
				}

			}
		viewer.refresh();

	}

	private void addContent() {
		if (person != null)
			list.add(person);
		else
			list.add(company);
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

}

/*
 * // private String name; // private String age; // private String cnp; //
 * private String owner;
 */

/*
 * // public AddHandler(TreeViewer viewer, String name, String age, String cnp,
 * String owner, // List<ChangeNameLater> list) { // this.viewer = viewer; //
 * this.name = name; // this.age = age; // this.cnp = cnp; // this.owner =
 * owner; // this.list = list; // }
 */
// private void create() {
//
// System.out.println(name + " " + cnp + " " + age);
// try {
// if (!name.isEmpty()) {
// if (cnp.length() == 13 && !age.isEmpty()) {
//
// person = new Person(name.toString(), Integer.parseInt(String.valueOf(age)),
// Long.parseLong(String.valueOf(cnp)));
// } else {
// person = null;
// }
//
// }
//
// } catch (Exception e) {
//
// person = null;
// }
//
// }
