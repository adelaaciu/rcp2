package com.apk.handler;

import java.util.List;

import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.apk.model.ChangeNameLater;
import com.apk.model.Company;
import com.apk.model.Person;

public class DeleteHandler implements SelectionListener {

	private TreeViewer viewer;
	private List<ChangeNameLater> list;

	public DeleteHandler(TreeViewer viewer, List<ChangeNameLater> persons) {
		this.viewer = viewer;
		this.list = persons;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {

		TreeSelection selection = (TreeSelection) viewer.getSelection();

		if (selection.isEmpty())
			list.removeAll(list);
		else {

			if (selection.iterator().next() instanceof Person) {

				Person person = (Person) selection.iterator().next();
				Person parent = person.getParent();

				if (parent != null) {
					List<Person> friends = person.getParent().getPersonFriends();
					friends.remove(person);
				} else if (parent == null)
					list.remove(person);

			} else {
				Company company = (Company) selection.iterator().next();
				Company parent = company.getParent();

				if (parent != null) {

					List<Company> companies = company.getParent().getCompanies();
					companies.remove(company);

				} else if (parent == null)

					list.remove(company);

			}
		}

		viewer.refresh();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}
}
