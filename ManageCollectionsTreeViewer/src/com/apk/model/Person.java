package com.apk.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author adela
 *
 */
public class Person extends ChangeNameLater {

	private Integer age;
	private Long cnp;
	private Person parent;
	private List<Person> personFriends = new ArrayList<>();

	public Person(String name, Integer age, Long cnp) {
		super(name);
		this.age = age;
		this.cnp = cnp;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Long getCnp() {
		return cnp;
	}

	public void setCnp(Long cnp) {
		this.cnp = cnp;
	}

	public List<Person> getPersonFriends() {
		return personFriends;
	}

	public void setPersonFriends(List<Person> personFriends) {
		this.personFriends = personFriends;
	}

	public Person getParent() {
		return parent;
	}

	public void setParent(Person parent) {
		this.parent = parent;
	}

	/*
	 * @Override public String toString() { return "Person firstName=" +
	 * firstName + ", lastName=" + lastName + ", age=" + age + ", cnp=" + cnp +
	 * ", parent=" + parent + ", personFriends=" + personFriends; }
	 */

}
