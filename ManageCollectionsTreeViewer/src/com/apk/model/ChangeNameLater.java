package com.apk.model;

public class ChangeNameLater {

	protected String name;

	public ChangeNameLater(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
