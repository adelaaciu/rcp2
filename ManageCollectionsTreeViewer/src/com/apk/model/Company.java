package com.apk.model;

import java.util.ArrayList;
import java.util.List;

public class Company extends ChangeNameLater {

	private String owner;
	private List<Company> companies = new ArrayList<>();
	private Company parent;
	public Company(String name, String owner) {
		super(name);
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public Company getParent() {
		return parent;
	}

	public void setParent(Company parent) {
		this.parent = parent;
	}
//
//	@Override
//	public String toString() {
//		return "Company [owner=" + owner + ", name=" + name + "]";
//	}

	
}
