package com.apk.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author adela
 *
 */
public class Person {

	private String firstName;
	private String lastName;
	private Integer age;
	private Long cnp;
	private Person parent;
	private List<Person> personFriends = new ArrayList<>();

	public static Person prototype() {
		return new Person("First Name", "Last Name", 0, 0L);
	}

	public Person() {
	}

	public Person(String firstName, String lastName, Integer age, Long cnp) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.cnp = cnp;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Long getCnp() {
		return cnp;
	}

	public void setCnp(Long cnp) {
		this.cnp = cnp;
	}

	public List<Person> getPersonFriends() {
		return personFriends;
	}

	public void setPersonFriends(List<Person> personFriends) {
		this.personFriends = personFriends;
	}

	public Person getParent() {
		return parent;
	}

	public void setParent(Person parent) {
		this.parent = parent;
	}

/*	@Override
	public String toString() {
		return "Person firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ", cnp=" + cnp
				+ ", parent=" + parent + ", personFriends=" + personFriends;
	}*/

}
