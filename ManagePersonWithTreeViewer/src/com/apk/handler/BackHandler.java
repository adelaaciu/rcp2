package com.apk.handler;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.internal.workbench.PartServiceImpl;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

public class BackHandler {

	@Execute
	public void execute(PartServiceImpl partService) {

		MPart part = partService.findPart("WellcomePart");
		part.setVisible(true);
		partService.getActivePart().setVisible(false);
	}
}
