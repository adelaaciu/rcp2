package com.apk.handler;

import java.util.List;

import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Text;

import com.apk.model.Person;

public class AddHandler implements SelectionListener {

	private Person person;
	private TreeViewer viewer;
	private Text firstName;
	private Text lastName;
	private Text age;
	private Text cnp;
	private List<Person> personsList;

	public AddHandler(TreeViewer viewer, Text firstName, Text lastName, Text age, Text cnp, List<Person> personsList) {
		this.viewer = viewer;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.cnp = cnp;
		this.personsList = personsList;
	}
	private void createPerson() {
		try {
			if (!firstName.getText().isEmpty() && !lastName.getText().isEmpty() && cnp.getText().length() == 13) {

				person = new Person(firstName.getText(), lastName.getText(),
						Integer.parseInt(String.valueOf(age.getText())), Long.parseLong(String.valueOf(cnp.getText())));
			} else
				person = null;
		} catch (Exception e) {
			person = null;
		}
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		createPerson();

		if (person != null) {
			if (personsList.isEmpty()) {
				personsList.add(person);

			} else {

				try {
					Person p = (Person) ((TreeSelection) viewer.getSelection()).iterator().next();
					person.setParent(p);
					p.getPersonFriends().add(person);
				} catch (Exception e1) {
					personsList.add(person);
				}

			}
		}
		viewer.refresh();

	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

}
