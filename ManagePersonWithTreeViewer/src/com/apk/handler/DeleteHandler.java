package com.apk.handler;

import java.util.List;

import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import com.apk.model.Person;

public class DeleteHandler implements SelectionListener {

	private TreeViewer viewer;
	private List<Person> persons;

	public DeleteHandler(TreeViewer viewer, List<Person> persons) {
		this.viewer = viewer;
		this.persons = persons;
	}

	@Override
	public void widgetSelected(SelectionEvent e) {

		try {
			Person person = (Person) ((TreeSelection) viewer.getSelection()).iterator().next();
			Person parent = person.getParent();
			if (parent != null) {
				List<Person> persons = person.getParent().getPersonFriends();
				persons.remove(person);
			} else if (parent == null)
				persons.remove(person);

		} catch (Exception event) {
			persons.removeAll(persons);
		}

		viewer.refresh();
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}
}
