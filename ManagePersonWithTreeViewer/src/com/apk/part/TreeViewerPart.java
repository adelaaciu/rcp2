package com.apk.part;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.apk.handler.AddHandler;
import com.apk.handler.DeleteHandler;
import com.apk.model.Person;
import com.apk.provider.PersonAgeLabelProvider;
import com.apk.provider.PersonCnpLabelProvider;
import com.apk.provider.PersonFirstNameLabelProvider;
import com.apk.provider.PersonLastNameLabelProvider;
import com.apk.provider.ViewContentProvider;

public class TreeViewerPart {

	@Inject
	private Composite parent;

	private Text firstNameText;
	private Text lastNameText;
	private Text ageText;
	private Text cnpText;
	private TreeViewer viewer;
	private List<Person> personsList = new ArrayList<>();
	private static final String FIRST_NAME = "First Name";
	private static final String LAST_NAME = "Last Name";
	private static final String AGE = "Age";
	private static final String CNP = "CNP";
	private static final String[] PROPERTIES = { FIRST_NAME, LAST_NAME, AGE, CNP };

	@PostConstruct
	public void create() {
		parent.setLayout(new GridLayout(4, false));

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FILL);
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 4;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;

		Tree tree = viewer.getTree();
		tree.setLayoutData(gridData);
		viewer.setContentProvider(new ViewContentProvider());

		tree.setHeaderVisible(true);

		addComponentsToTreeViewer(viewer);
		addContent();
		addCellModifier(tree);

		Button addBtn = new Button(parent, SWT.CENTER);
		addBtn.setText("Add");
		addBtn.addSelectionListener(new AddHandler(viewer, firstNameText, lastNameText, ageText, cnpText, personsList));

		Button deleteBtn = new Button(parent, SWT.CENTER);
		deleteBtn.setText("Delete");
		deleteBtn.addSelectionListener(new DeleteHandler(viewer, personsList));

		// when double click the editing happen
		TreeViewerEditor.create(viewer, new ColumnViewerEditorActivationStrategy(viewer) {
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.MOUSE_DOUBLE_CLICK_SELECTION;
			}
		}, 0);

	}

	private void addCellModifier(Tree tree) {

		viewer.setColumnProperties(PROPERTIES);
		viewer.setInput(personsList);

		CellEditor[] editors = new CellEditor[4];

		editors[0] = new TextCellEditor(tree);
		editors[1] = new TextCellEditor(tree);
		editors[2] = new TextCellEditor(tree);
		editors[3] = new TextCellEditor(tree);

		viewer.setCellEditors(editors);

		viewer.setCellModifier(new ICellModifier() {

			@Override
			public void modify(Object element, String property, Object value) {

				TreeItem treeItem = (TreeItem) element;
				Person p = (Person) treeItem.getData();

				switch (property) {
				case FIRST_NAME:
					p.setFirstName(value.toString());
					break;
				case LAST_NAME:
					p.setLastName(value.toString());
					break;
				case AGE:
					try {
						p.setAge(Integer.parseInt(value.toString()));
					} catch (Exception e) {
						// TODO: handle exception
					}
					break;
				case CNP:
					try {
						if (value.toString().length() == 13)
							p.setCnp(Long.parseLong(value.toString()));
					} catch (Exception e) {
						// TODO: handle exception
					}

					break;
				}
				viewer.update(p, PROPERTIES);
			}

			@Override
			public Object getValue(Object element, String property) {

				Person p = (Person) element;

				switch (property) {
				case FIRST_NAME:
					return p.getFirstName();
				case LAST_NAME:
					return p.getLastName();
				case AGE:
					return p.getAge().toString();
				case CNP:
					return p.getCnp().toString();

				}

				return "";
			}

			@Override
			public boolean canModify(Object element, String property) {
				return true;
			}
		});
	}

	private void addComponentsToTreeViewer(TreeViewer viewer) {
		TreeViewerColumn mainColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		mainColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new PersonFirstNameLabelProvider()));
		mainColumn.getColumn().setText(FIRST_NAME);
		mainColumn.getColumn().setWidth(100);

		TreeViewerColumn lastNameColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		lastNameColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new PersonLastNameLabelProvider()));
		lastNameColumn.getColumn().setText(LAST_NAME);
		lastNameColumn.getColumn().setWidth(100);

		TreeViewerColumn ageColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		ageColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new PersonAgeLabelProvider()));
		ageColumn.getColumn().setText(AGE);
		ageColumn.getColumn().setWidth(100);

		TreeViewerColumn cnpColumn = new TreeViewerColumn(viewer, SWT.CENTER);
		cnpColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new PersonCnpLabelProvider()));
		cnpColumn.getColumn().setText(CNP);
		cnpColumn.getColumn().setWidth(100);

		new Label(parent, SWT.CENTER).setText("FirstName");
		firstNameText = new Text(parent, SWT.CENTER);

		new Label(parent, SWT.CENTER).setText("LastName");
		lastNameText = new Text(parent, SWT.CENTER);

		new Label(parent, SWT.CENTER).setText("Age");
		ageText = new Text(parent, SWT.CENTER);
		ageText.setTextLimit(3);
		ageText.setToolTipText("Just digits");

		new Label(parent, SWT.CENTER).setText("CNP");
		cnpText = new Text(parent, SWT.CENTER);
		cnpText.setTextLimit(13);
		cnpText.setToolTipText("Must be 13 digits");

		/**
		 * Foreach column
		 */
		// setEditingSuport(viewer, mainColumn, lastNameColumn, ageColumn,
		// cnpColumn);
	}

	private void setEditingSuport(TreeViewer viewer, TreeViewerColumn mainColumn, TreeViewerColumn lastNameColumn,
			TreeViewerColumn ageColumn, TreeViewerColumn cnpColumn) {
		mainColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				Person p = (Person) element;
				p.setFirstName(String.valueOf(value));
				getViewer().update(element, null);

			}

			@Override
			protected Object getValue(Object element) {
				return ((Person) element).getFirstName();
			}

			@Override
			protected CellEditor getCellEditor(Object element) { //
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) { // TODO
				return true;
			}
		});
		lastNameColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				Person p = (Person) element;
				p.setLastName(String.valueOf(value));
				getViewer().update(element, null);

			}

			@Override
			protected Object getValue(Object element) {
				return ((Person) element).getLastName();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		ageColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				Person p = (Person) element;
				p.setAge(Integer.parseInt(String.valueOf(value)));
				getViewer().update(element, null);

			}

			@Override
			protected Object getValue(Object element) {
				return ((Person) element).getAge().toString();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) {
				// TODO Auto-generated method stub
				return true;
			}
		});

		cnpColumn.setEditingSupport(new EditingSupport(viewer) {

			@Override
			protected void setValue(Object element, Object value) {
				Person p = (Person) element;
				p.setCnp(Long.parseLong(String.valueOf(value)));
				getViewer().update(element, null);

			}

			@Override
			protected Object getValue(Object element) {
				return ((Person) element).getCnp().toString();
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				// TODO Auto-generated method stub
				return new TextCellEditor((Composite) getViewer().getControl());
			}

			@Override
			protected boolean canEdit(Object element) {
				// TODO Auto-generated method stub
				return true;
			}
		});
	}

	private void addContent() {
		Person p = new Person("Adela", "Aciu", 21, 2950425056066L);

		Person p1 = new Person("Amalia", "Aciu", 21, 2950101076066L);
		Person p2 = new Person("Alex", "Aciu", 23, 1950101076066L);
		Person p3 = new Person("Marian", "Onac", 22, 1951006076066L);
		Person p5 = new Person("George", "Onac", 22, 1951006076066L);

		List<Person> friends = new ArrayList<>();
		friends.add(p1);
		friends.add(p2);
		friends.add(p3);

		p.setPersonFriends(friends);

		p1.setParent(p);
		p2.setParent(p);
		p3.setParent(p);

		friends = new ArrayList<>();
		friends.add(p5);
		p3.setPersonFriends(friends);
		p5.setParent(p3);

		personsList.add(p);
	}

}
