package com.apk.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.internal.workbench.PartServiceImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class WellcomePart {

	@Inject
	private Composite parent;

	@PostConstruct
	public void createPart(PartServiceImpl partService) {

		parent.setLayout(new GridLayout(1, false));

		Button startBtn = new Button(parent, SWT.CENTER);
		startBtn.setText("START");
		startBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				partService.findPart("TreeViewerPart").setVisible(true);
				partService.getActivePart().setVisible(false);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

}
