package com.apk.provider;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.apk.model.Person;

public class ViewContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		List<Person> list = (List<Person>)inputElement;
		return list.toArray();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		Person person = (Person) parentElement;
		return person.getPersonFriends().toArray();
	}

	@Override
	public Object getParent(Object element) {
		Person person = (Person) element;
		return person.getParent();
	}

	@Override
	public boolean hasChildren(Object element) {
		Person person = (Person) element;
		return !person.getPersonFriends().isEmpty();
	}
}
