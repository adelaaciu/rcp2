package com.apk.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;

import com.apk.model.Person;

public class PersonLastNameLabelProvider extends LabelProvider implements IStyledLabelProvider {

	@Override
	public StyledString getStyledText(Object element) {
		// TODO Auto-generated method stub
		Person person = (Person) element;
		String name = person.getLastName();

		return new StyledString(name);
	}

}
