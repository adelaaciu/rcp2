package com.example.startapi;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.example.jface.table.viewer.View;
import com.example.jface.tableviewer.model.Person;

public class Start {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		shell.setSize(550, 550);
		Composite parent = new Composite(shell, SWT.NONE);
		parent.setSize(550, 550);
		View view = new View();

		Text searchText;

		view.createPartControl(parent);

		TableViewer viewer = view.getViewer();

//		searchText = view.getSearchText();


		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}

}

/**
 * 
 * 
 * 
 *		searchText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				if (searchText.getText().isEmpty()) {
					viewer.refresh();
					viewer.setInput(personList);
				} else {

					viewer.refresh();
					viewer.setInput(getList(searchText.getText()));
				}
			}

		});
 */
/***
 * 
 * 
 * 	private List<Person> getList(String firstName) {
		List<Person> list = new ArrayList<>();

		personList.forEach(x -> {
			if (x.getFirstName().equals(firstName))
				list.add(x);
		});

		return list;

	}
 * 
 * 
 * */
