package com.example.combo.viewer;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class StartApplication {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setLayout(new FormLayout());

		Composite parent = new Composite(shell, SWT.NONE);

		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);

		Label label = new Label(parent, SWT.NONE);
		label.setText("Select a person:");
		
		final ComboViewer viewer = new ComboViewer(parent, SWT.READ_ONLY);

		viewer.setContentProvider(ArrayContentProvider.getInstance());
	
		viewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Person) {
					Person person = (Person) element;
					return person.getFirstName();
				}
				return super.getText(element);
			}
		});

		Person[] persons = new Person[] { new Person("Lars", "Vogel"), new Person("Tim", "Taler"),
				new Person("Jim", "Knopf") };

		viewer.setInput(persons);

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				if (selection.size() > 0) {
					System.out.println(((Person) selection.getFirstElement()).getLastName());
				}
			}
		});
		// sa apara ca si selectie prima persoana automat
		// you can select an object directly via the domain object
		Person person = persons[0];
		viewer.setSelection(new StructuredSelection(person));

		// retrieves the selection, returns the data model object
		IStructuredSelection selection = viewer.getStructuredSelection();
		Person p = (Person) selection.getFirstElement();

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}
}
