package part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

public class Part {

	@Inject
	private Shell shell;

	@PostConstruct
	private void createPart() {

		FormLayout layout = new FormLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 5;
		shell.setLayout(layout);

		Button button1 = new Button(shell, SWT.PUSH);
		Button button2 = new Button(shell, SWT.PUSH);
		Button button3 = new Button(shell, SWT.PUSH);
		Button button4 = new Button(shell, SWT.PUSH);
		Button button5 = new Button(shell, SWT.PUSH);

		button1.setText("B1");
		button2.setText("Wide button 2");
		button3.setText("Button 3");
		button4.setText("B4");
		button5.setText("Button 5");

		FormData data1 = new FormData();
		data1.left = new FormAttachment(0, 5);
		data1.right = new FormAttachment(25, 0);
		button1.setLayoutData(data1);

		FormData data2 = new FormData();
		data2.left = new FormAttachment(button1, 5);
		data2.right = new FormAttachment(100, -5);
		button2.setLayoutData(data2);

		FormData data3 = new FormData(60, 60);
		data3.top = new FormAttachment(button1, 5);
		data3.left = new FormAttachment(50, -30);
		data3.right = new FormAttachment(50, 30);
		button3.setLayoutData(data3);

		FormData data4 = new FormData();
		data4.top = new FormAttachment(button3, 5);
		data4.bottom = new FormAttachment(100, -5);
		data4.left = new FormAttachment(25, 0);
		button4.setLayoutData(data4);

		FormData data5 = new FormData();
		data5.bottom = new FormAttachment(100, -5);
		data5.left = new FormAttachment(button4, 5);
		button5.setLayoutData(data5);
	
	
	}

}
