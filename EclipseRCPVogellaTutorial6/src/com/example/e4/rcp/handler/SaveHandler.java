package com.example.e4.rcp.handler;

import org.eclipse.e4.core.di.annotations.Execute;

public class SaveHandler {
	@Execute
	public void execute() {
		System.out.println((this.getClass().getSimpleName() + " called"));
	}
}
