package com.rowdatra.example.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

public class RowDataExamplePart {

	@Inject
	private Shell shell;

	@PostConstruct
	public void create() {

		shell.setLayout(new RowLayout());

		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("Button1");
		button1.setLayoutData(new RowData(50, 40));

		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Button1");
		button2.setLayoutData(new RowData(50, 30));

		Button button3 = new Button(shell, SWT.PUSH);
		button3.setText("Button1");
		button3.setLayoutData(new RowData(50, 20));
	}

}
