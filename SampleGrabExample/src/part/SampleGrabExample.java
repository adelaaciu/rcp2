package part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

public class SampleGrabExample {

	@Inject
	private Composite parent;

	@PostConstruct
	private void construct() {
		parent.setLayout(new GridLayout(2, false));

		Label nameLabel = new Label(parent, SWT.NONE);
		nameLabel.setText("Name");

		Text nameText = new Text(parent, SWT.BORDER);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		nameText.setLayoutData(gridData);
		nameText.setText("Text grows horizontally");

		Label addressLabel = new Label(parent, SWT.NONE);
		addressLabel.setText("Address:");
		gridData = new GridData();
		gridData.verticalAlignment = SWT.TOP;
		addressLabel.setLayoutData(gridData);

		Text addressText = new Text(parent, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		addressText.setLayoutData(gridData);
		addressText.setText("This text field and the List\nbelow share any excess space.");

		Label suportsLabel = new Label(parent, SWT.NONE);
		suportsLabel.setText("Sports played:");
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		suportsLabel.setLayoutData(gridData);

		List sportsList = new List(parent, SWT.BORDER | SWT.MULTI);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessVerticalSpace = true;
		sportsList.setLayoutData(gridData);
		sportsList.add("Hockey");
		sportsList.add("StreetHockey");

	}

}
