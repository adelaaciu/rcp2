package handeler;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.Shell;

public class SaveHandler {

	@CanExecute
	public boolean canExecutee(EPartService partService) {
		if (partService != null)
			return !partService.getDirtyParts().isEmpty();

		return false;
	}

	@Execute
	public void execute(EPartService partService) {
		partService.saveAll(false);
	}

}
