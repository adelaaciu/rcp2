package handeler;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import part.EditPart;

public class UndoHandler {

	private EditPart editPart;
	private String selectedText = "";

	@CanExecute
	public boolean canExecutee(MPart part) {
		if (part != null) {
			editPart = (EditPart) part.getObject();
			if (!editPart.getTxtInput().getText().equals(selectedText))
				return true;
		}

		return false;
	}

	@Execute
	public void execute(MPart part) {
		if (part != null) {
			editPart = (EditPart) part.getObject();

			editPart.getSecondTxtInput().setText(selectedText);
			selectedText = editPart.getTxtInput().getText();

		}
	}

}
