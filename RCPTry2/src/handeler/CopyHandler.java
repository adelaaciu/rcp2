package handeler;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import part.EditPart;

public class CopyHandler {

	@Execute
	public void execute(MPart part) {
		if (part != null) {
			EditPart editPart = (EditPart) part.getObject();
			String selectedText = editPart.getTxtInput().getSelectionText();

			editPart.getSecondTxtInput().setText("Copy action " + selectedText);

		}
	}

}
