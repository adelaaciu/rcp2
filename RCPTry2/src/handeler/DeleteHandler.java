package handeler;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.widgets.Text;

import part.EditPart;

public class DeleteHandler {

	@Execute
	public void execute(MPart part) {
		if (part != null) {
			EditPart editPart = (EditPart) part.getObject();
			editPart.getTxtInput().setText("");

		}
	}

}
