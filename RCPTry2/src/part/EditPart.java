package part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class EditPart {

	private Text txtInput;
	private Text secondTxtInput;

	@Inject
	private MDirtyable dirty;

	@Inject
	public EditPart() {
	}

	@PostConstruct
	public void createComposite(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		txtInput = new Text(parent, SWT.BORDER);
		txtInput.setMessage("Enter text to mark part as dirty");
		txtInput.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				dirty.setDirty(true);

			}
		});
		txtInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		secondTxtInput = new Text(parent, SWT.BORDER);
		secondTxtInput.setMessage("Modified");
		secondTxtInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}

	@Focus
	public void setFocus() {
		txtInput.setFocus();
	}

	@Persist
	public void save() {
		dirty.setDirty(false);
	}

	public Text getTxtInput() {
		return txtInput;
	}

	public Text getSecondTxtInput() {
		return secondTxtInput;
	}

}
