package com.layout.example.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class FillLayoutExample {

	@Inject
	private Composite parent;


	@PostConstruct
	public void create() {
		FillLayout fillLayout = new FillLayout();
		fillLayout.type = SWT.VERTICAL;
		parent.setLayout(fillLayout);
		new Button(parent, SWT.PUSH).setText("B1");
		new Button(parent, SWT.PUSH).setText("Wide Button 2");
		new Button(parent, SWT.PUSH).setText("Button 3");
	}
}
