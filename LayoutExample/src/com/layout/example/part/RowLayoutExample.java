package com.layout.example.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class RowLayoutExample {

	@Inject
	private Composite parent;

	@PostConstruct
	public void create() {
		RowLayout rowLayout = new RowLayout();
//		rowLayout.wrap = false;
//		rowLayout.pack = false;
//		rowLayout.justify = true;
//		rowLayout.type = SWT.VERTICAL;
//		rowLayout.marginLeft = 5;
//		rowLayout.marginTop = 5;
//		rowLayout.marginRight = 5;
//		rowLayout.marginBottom = 5;
//		rowLayout.spacing = 0;
//		
		
		
		rowLayout.wrap = false;
		rowLayout.pack = true;
		rowLayout.justify = false;
		rowLayout.type = SWT.HORIZONTAL;
		//rowLayout.justify = true;
		rowLayout.type = SWT.VERTICAL;
				
		parent.setLayout(rowLayout);

		new Button(parent, SWT.PUSH).setText("B1");
		new Button(parent, SWT.PUSH).setText("Wide Button 2");
		new Button(parent, SWT.PUSH).setText("Button 3");

	}
}
