package com.example.jtable.simple;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableColumn;

public class FirstExample {

	public static void main(String[] args) {

		GridLayout layout = new GridLayout(3, false);
		Display display = new Display();

		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		Composite parent = new Composite(shell, SWT.NONE);

		parent.setVisible(true);
		parent.setLayout(layout);

		TableViewer viewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		viewer.setContentProvider(ArrayContentProvider.getInstance());

		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

		TableViewerColumn colFirstName = new TableViewerColumn(viewer, SWT.BOLD | SWT.ITALIC);
		TableColumn column = colFirstName.getColumn();
		colFirstName.getColumn().setText("First name");
		colFirstName.getColumn().setWidth(100);
		colFirstName.getColumn().setResizable(true);
		colFirstName.getColumn().setMoveable(true);
		colFirstName.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;

				return p.getFirstName();
			}
		});

		TableViewerColumn colMarried = new TableViewerColumn(viewer, SWT.BOLD);
		colMarried.getColumn().setWidth(100);
		colMarried.getColumn().setText("Married");
		colMarried.getColumn().setResizable(true);
		colMarried.getColumn().setMoveable(true);
		colMarried.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {

				Person p = (Person) element;

				if (p.isMarried())
					return "Yes";
				else
					return "No";

			}

		});

		Person[] persons = new Person[] { new Person("Lars", "Vogel"), new Person("Tim", "Taler"),
				new Person("Jim", "Knopf") };

		viewer.setInput(persons);
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// **** Trebuie adaugat pentru a vizualiza capul de tabel
		viewer.getTable().setHeaderVisible(true);
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111
		parent.pack();
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();

	}

}
