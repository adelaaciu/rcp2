package com.nattable.first.example.part;

import javax.annotation.PostConstruct;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.data.IColumnPropertyAccessor;
import org.eclipse.nebula.widgets.nattable.data.IDataProvider;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.data.ReflectiveColumnPropertyAccessor;
import org.eclipse.nebula.widgets.nattable.layer.DataLayer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.nattable.first.example.model.Person;
import com.nattable.first.example.model.PersonService;

public class NetTableDataExamplePart {

	@PostConstruct
	public void postConstruct(Composite parent) {
		parent.setLayout(new GridLayout());

		// property names of the Person class
		String[] propertyNames = { "firstName", "lastName", "gender", "married", "birthday" };

		// create the data provider
		IColumnPropertyAccessor<Person> columnPropertyAccessor = new ReflectiveColumnPropertyAccessor<Person>(
				propertyNames);
		IDataProvider bodyDataProvider = new ListDataProvider<Person>(PersonService.getPersons(10),
				columnPropertyAccessor);

		final DataLayer bodyDataLayer = new DataLayer(bodyDataProvider);

		final NatTable natTable = new NatTable(parent, SWT.NO_REDRAW_RESIZE | SWT.DOUBLE_BUFFERED | SWT.BORDER,
				bodyDataLayer);

		GridDataFactory.fillDefaults().grab(true, true).applyTo(natTable);
	}

}
