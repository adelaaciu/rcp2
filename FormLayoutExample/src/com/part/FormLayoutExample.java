package com.part;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class FormLayoutExample {

	@Inject
	private Shell shell;

	@Inject
	private Display display;

	@PostConstruct
	private void create() {
		FormLayout layout = new FormLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		shell.setLayout(layout);
		FormData formData = new FormData();

		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("B1");
		// coord referitoare la fereastra

		formData.top = new FormAttachment(0, 60);
		formData.bottom = new FormAttachment(100, -5);
		formData.left = new FormAttachment(20, 0);
		formData.right = new FormAttachment(100, -3);

		button1.setLayoutData(formData);

//******************************************************
		formData = new FormData();
		formData.top = new FormAttachment(0, 60);
		formData.left = new FormAttachment(20, 0);

		button1.setLayoutData(formData);
//*****************************************************
		formData = new FormData();
		formData.width = 30;
		formData.top = new FormAttachment(0, 60);
		formData.bottom = new FormAttachment(100, -5);
		formData.left = new FormAttachment(20, 0);
		button1.setLayoutData(formData);
//******************************************************
		formData = new FormData();
		formData.top = new FormAttachment(30, 70, 10);
		button1.setLayoutData(formData);
//******************************************************
		formData = new FormData();
		formData.right = new FormAttachment(100, -5);
		button1.setLayoutData(formData);
//******************************************************
		formData = new FormData();
		formData.top = new FormAttachment(20, 0);
		button1.setLayoutData(formData);

		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Wide button2");
		FormData formData2 = new FormData();
		formData2.top = new FormAttachment(button1, 10);
		button2.setLayoutData(formData2);
//******************************************************
		formData = new FormData(50, 50);
		formData.top = new FormAttachment(20, 0);
		button1.setLayoutData(formData);

		formData2 = new FormData();
		formData2.left = new FormAttachment(button1, 5);
		formData2.top = new FormAttachment(button1, 0, SWT.TOP);
		button2.setLayoutData(formData2);
		
		
	}

}
