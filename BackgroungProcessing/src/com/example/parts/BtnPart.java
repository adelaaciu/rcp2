package com.example.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import com.example.listener.MySelectionAdapter;

public class BtnPart {

	@Inject
	private Shell shell;

	@PostConstruct
	public void postConstruct(Composite parent) {
		shell.setBounds(100,100,200,200);
		
		Button button = new Button(parent, SWT.PUSH);
		button.addSelectionListener(new MySelectionAdapter(shell)); 
	}

}
