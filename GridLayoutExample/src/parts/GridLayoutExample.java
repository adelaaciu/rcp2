package parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;

public class GridLayoutExample {

	@Inject
	private Shell shell;

	@PostConstruct
	private void create() {

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		shell.setLayout(gridLayout);

		Button b1 = new Button(shell, SWT.PUSH);
		b1.setText("B1");

		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		b1.setLayoutData(gridData);

		Button b2 = new Button(shell, SWT.PUSH);
		b2.setText("Wide Button 2");

		Button b3 = new Button(shell, SWT.PUSH);
		b3.setText("Button 3");

		gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.verticalSpan = 2;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		
		b3.setLayoutData(gridData);
		
		
		Button b4 = new Button(shell, SWT.PUSH);
		b4.setText("B4");
		gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		b4.setLayoutData(gridData);

		Button b5 = new Button(shell, SWT.PUSH);
		b5.setText("Button 5");
		
	}
}
